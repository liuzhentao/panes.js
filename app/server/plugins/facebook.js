'use strict';
/**
 * init facebook passport plugin
 */

 var passport = require('passport'),
   	 FacebookStrategy = require('passport-facebook').Strategy;

module.exports = function(panes , app , config )
{

	var fb = config.passport.facebook;

	passport.use(new FacebookStrategy({
    		clientID: fb.clientID,
    		clientSecret: fb.clientSecret,
    		callbackURL: fb.callbackURL
    	},
    	function(accessToken , refreshToken , profile , done)
    	{
    		var user = {};
    		// this is where we do our login or store the user facebook data to the db
    		// @TODO
    		done(null , user);
    	})
    );

};
