'use strict';
/**
 * init the twitter passport plugin
 */
 var passport = require('passport'),
   	 TwitterStrategy = require('passport-twitter').Strategy;


module.exports = function(panes , app , config)
{
	var tw = config.passport.twitter;

	passport.use(new TwitterStrategy({
    		consumerKey: tw.consumerKey,
    		consumerSecret: tw.consumerSecret,
    		callbackURL: tw.callbackURL
    	}, function(token, tokenSecret , profile , done) {
    		var user = {};
    		// @TODO
    		done(null , user);
    	})
    );

};
