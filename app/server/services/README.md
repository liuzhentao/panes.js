#app/server/lib folder explanation

The idea of panes.js is to give you the awesome dude (lady) maximum flexiblity to create awesome application.

At the same time, let you code as little as possible to do most of the work.

In this folder, we have files that will allow you to overwrite our own implementation. Without hacking the core
of our own framework.

#available hooks

1. jwt_unauthorized_handler:

	// this will pass into the middleware chain
	module.exports = function(err , req , res , next)
	{

	}

2. jwt_handler:

example:

    module.exports = function(jwtConfig)
    {
	    // expect return an express middleware
		return function(req , res , next)
	    {
		    // do your thing here
	    }
    };
