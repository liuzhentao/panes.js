'use strict';

/**
 * PANES.js no longer use the MEAN.js style one route file match one controller style
 * Instead, we use route table + dynamic route , rules as follow
 * if we specify particular route here, it will get match first
 * then we use a common pattern /:controller/:action/* to first try to match route
 * if nothing match we just next() and let the rest of the app handle 404 or 500
 */

module.exports = [
    {route: '/' , controller: 'main' , action: 'index'},
    {route: '/login' , controller: 'login' , action: 'index'},
    {route: '/login/facebook' , passport: {provider: 'facebook'} , listen: 'get' },
    {route: '/login/twitter' , passport: {provider: 'twitter'} , listen: 'get'},
    {route: '/login/callback/facebook' , passport: {
                                        provider: 'facebook' ,
                                        params: {succcessRedirect: '/' , failureRedirect: '/login'}
                                    },
                                    listen: 'get'
    },
    {route: '/login/callback/twitter' , passport: {
                                        provider: 'twitter',
                                        params: {succcessRedirect: '/' , failureRedirect: '/login'}
                                    },
                                listen: 'get'
    }
];
