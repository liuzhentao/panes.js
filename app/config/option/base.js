'use strict';

/**
 * global available variables
 */
module.exports = {
    app: {
        name: 'PANES.JS DEMO',
        metaTitle: 'PANES.JS',
        metaDescription: 'Panes.js is a node.js MVC framework',
        metaKeywords: 'mongodb, express, angularjs, node.js, mongoose, passport',
        metaLang: 'en'
    },
    port: process.env.PORT || 3001,
    proxy: false,
    cpu: 1,
    service: {
        email: {
            from: process.env.MAILER_FROM || 'MAILER_FROM',
            options: {
                service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
                auth: {
                    user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
                    pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
                }
            }
        },
        auth: {
            secret: 'tecKs4ev4myl'
        }, // production use a more secure 32 bytes key
        log: {
            // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
            format: 'combined',
            // Stream defaults to process.stdout
            // Uncomment to enable logging to a log on the file system
            options: {
                stream: 'access.log'
            }
        },
        template: {
            engine: 'swig',
            name: '.html',
        }
    },
    plugin: {}
};
