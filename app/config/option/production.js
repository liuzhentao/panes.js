'use strict';
/**
 * production.js
 * those assets options shouldn't change because it affect the gulp build script
 * if you change those name in the gulpfile.js then you MUST change here also.
 */
module.exports = {
    datasource: {
        'default': {
            driver: 'mongo',
            uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || '192.168.33.15') + '/mean',
            options: {
                // you might want to pass this
                // server: {socketOptions: { keepAlive: 1 } }
                // replset: {socketOptions: { keepAlive: 1 } }
            }
        }
    },
    jwt: {secret: 'geEm<duM,Ne[Ob+quoM.Nu,Ot#nI}olj'},
    assets: {
        css: [
            'css/vendor.min.css'
            'css/styles.min.css'
        ],
        js: [
            'js/vendor.min.js',
            'js/app.min.js'
        ]
    }
};

// -- EOF --
