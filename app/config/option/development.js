'use strict';
/**
 * development.js during the development we use bower-files to get the list of all the lib(css/js) files
 */
module.exports = {
    datasource: {
        /*
        'default': { // breaking change 14.9.2015 - support multiple database profile
            type: 'NOSQL', // use type to describe model so we could support more in the future
            driver: 'mongo',
            uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || '192.168.33.15') + '/mean',
            options: {
                user: '',
                pass: ''
            }
        },
        */
        'default': { // only the above default is require, any other profile will have name paramters
            type: 'SQL', // instead of driver name , use a more generic type to describe the Model
            driver: 'sqlite',
            connStr: 'data/test.db', // [driver]://[username]:[password]@[url]:[port]/[database name]
            options: {}
        }
    },
    assets: {
        lib: {
            // The framework automatically inject files from bower, you can add extra below
            // they will load after the core bower lib
            // css:  'add your own special css file here'
            // js:   'add your own js lib files here'
        },
        // this should be standard procedure. Don't change
        css: [
            'app/client/web/styles/**/*.css'
        ],
        js: [
            'app/client/web/scripts/**/*.js'
        ],
        tests: [
            'app/client/vendor/bower_components/angular-mocks/angular-mocks.js',
            'app/client/web/test/modules/*/tests/*.js'
        ]
    },
    /***************************************
    This is a very bad example, you should
    put this sensitive info in a `local.js`
    in this same folder. And it will get
    picked up by configurator. local.js
    is automatically exclude from gitignore
    ***************************************/
    plugin: {
        passport: {
    		facebook: {
    			clientID: '4398349382904',
    			clientSecret: 'sdjf98sfklsjklfds',
    			callbackURL: 'https://demo.panesjs.com/login/callback/facebook'
    		},
    		twitter: {
    			consumerKey: 'sjdksl3k4j32908',
    			consumerSecret: 'skjfsd90809sf',
    			callbackURL: 'https://demo.panesjs.com/login/callback/twitter'
    		},
            local: {
                username: 'username',
                password: 'password'
            }
    	}
    }
};
