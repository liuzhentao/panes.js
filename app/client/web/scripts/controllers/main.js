'use strict';

/**
 * @ngdoc function
 * @name stagingApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the stagingApp
 */
angular.module('panesjsApp').controller('MainCtrl',['$scope' , '$http' , function ($scope , $http ) {

    var self = this;

    self.busy = true;
    self.items = [];

    var temp = [];

    self.nextPage = function()
    {
        self.items = temp;
    };

    self.loader = function()
    {
        $http.get('main/test').success(function(data)
        {
            self.busy = false;
            temp = data.data;
        });
    };

    self.removeItem = function(item)
    {
        console.log('call remove item' , item);
    };

    self.loader();

    self.isMoreOrNone = 'Load more ...';

}]);
