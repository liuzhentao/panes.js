
/* global angular , window */
'use strict';


var appName = 'panesjsApp';

//Start by defining the main module and adding the module dependencies
angular.module(appName,  ['ngAnimate',
                           'ngCookies',
                           'ngMessages',
                           'ngResource',
                           'ngRoute',
                           'ngSanitize',
                           'ngTouch',
                           'angular-jwt'])

.config(['$routeProvider' , '$locationProvider' ,'$httpProvider' , 'jwtInterceptorProvider', function ($routeProvider , $locationProvider , $httpProvider ,jwtInterceptorProvider) {

    /**
     * then we won't call the jwt when we just fetching html
     */
    jwtInterceptorProvider.tokenGetter = ['config', function(config) {
        // Skip authentication for any requests ending in .html
        if (config.url.substr(config.url.length - 5) == '.html') {
            return null;
        }
        return localStorage.getItem('id_token');
     }];

    $httpProvider.interceptors.push('jwtInterceptor');

	$routeProvider
		.when('/', {
			templateUrl: 'views/main.html',
			controller: 'MainCtrl',
			controllerAs: 'main'
		})
		.otherwise({
			redirectTo: '/'
		});
}])

/**
 * @ngdoc overview
 * @name stagingApp
 * @description
 * # stagingApp
 *
 * Main module of the application.
 */
.run(['$rootScope' ,'$window' , function($rootScope , $window)
{
    // put global stuff here if you want ...
}]);

//Then define the init function for starting up the application
angular.element(document).ready(function()
{
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') {
		window.location.hash = '#!';
	}
	//Then init the app
	angular.bootstrap(document, [appName]);
});


// -- EOF -- //
