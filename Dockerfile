#TODO: Need a hell lot more work to get this to work! (4 Aug 2015)

FROM node

MAINTAINER Joel Chu, joelchu@foxmail.com

WORKDIR /home/panesjs

# Install panes.JS Prerequisites
RUN npm install -g gulp
RUN npm install -g bower

# Install panes.JS packages
ADD package.json /home/panesjs/package.json
RUN npm install

# Manually trigger bower. Why doesnt this work via npm install?
ADD .bowerrc /home/panesjs/.bowerrc
ADD bower.json /home/panesjs/bower.json
RUN bower install --config.interactive=false --allow-root

# Make everything available for start
ADD . /home/panesjs

# currently only works for development
ENV NODE_ENV development

# Port 3001 for server
# Port 35729 for livereload
EXPOSE 3001 35729
CMD ["gulp"]
