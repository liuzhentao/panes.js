'use strict';
/**
 * main controller for testing
 */

import {Controller} from 'panes';

export default class main extends Controller {

	index() {
		this.$render({'greeting': 'Hello world!' ,
					  'msg': 'When you see this - meaning, you have correctly setup panes.js!'});
	}

}
