'use strict';
/**
 * login action
 */

import {Controller} from 'panes';

export default class Login extends Controller {

	index()
	{
		let data = {
				metaTitle: 'Please login',
				metaDescription: 'demo to show the login',
				msg: 'Please login'
			};

		this.$render(data , 'login');

	}

}
