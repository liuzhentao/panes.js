/**
 * V.0.0.2 start from clean
 * from: http://npm.taobao.org/package/gulp-express2
 */

var gulp = require('gulp');
var express = require('express');
var livereload = require('connect-livereload');
var open = require('gulp-open');
var gutil = require('gulp/node_modules/gulp-util');
var chalk = require('chalk');
var exec = require('child_process').exec;
var app = express();
/**
 * here are couple choices,
 * 1. let the user edit this and overwrite
 * 2. when we use the generator we could try to search and find it
 * 3. use something else?
 */
var browser = '/Applications/dart/chromium/Chromium.app';

app.use(livereload({ port: 35729 , interval: 1000}));

gulp.task('watch', function (cb) {

	exec('cd vagrant && vagrant up' , function()
    {
		var command = 'open -a  ' + browser + ' http://localhost:3001';
		exec(command , function(err , stdout , stderr)
		{
			if (err) {
				console.log('err' , chalk.red(err));
			}
			if (stdout) {
				console.log('stdout' , chalk.blue(stdout));
			}
			if (stderr) {
				console.log('stderr' , chalk.green(stderr));
			}
			cb();
		});
    });

	var errLogger = function () {
        gutil.log(gutil.colors.red.apply(undefined, arguments));
    };
    var app = require('gulp-express2')('start.js', gutil.log, errLogger);

    app.run();

	// client side
	gulp.watch([
		'./app/client/**/*.*'
	]).on('change' , function(file)
	{
		app.notify(file);
	});

	// server side
    gulp.watch([
		'./lib/**/*.*',
		'./app/config/**/*.*',
        './app/server/**/*.*'
    ]).on('change', function (file) {

		app.run();
		app.notify(file);
    });
/*
    gulp.watch([
        './app/client/less /* * /*.less'
    ]).on('change', function (file) {
        gulp.start('less');
        app.notify(file);
    });
*/
});

gulp.task('less' , function()
{

});

gulp.task('sass' , function()
{

});

gulp.task('build' , function()
{

});
