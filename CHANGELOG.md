panes.js change log

2015-09-19 (0.6.1) breaking change, the way how the controller and model get generate will be wrap around in our main `panes.controller` and `panes.model` object factory 
2015-08-19 (0.5.0) introducing hooks for overwrite our own core functions
2015-08-18 (0.4.0) replace all the front end code  
2015-08-17 (0.3.5) replace session with jwt token
2015-08-16 (0.3.0) rewrite the entire router
2015-08-15 (0.2.0) add RDM support
2015-08-14 (0.1.0) basic controller
2015-08-12 (0.0.1) setup the basic mvc with a different structure  
